<?php

use Illuminate\Http\Request;
use App\Models\TeamList;
use App\Http\Resources\TeamhMatchesCollection;

Route::group([
    'prefix' => '/'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@signup');

        Route::get('/team/members', 'Api\TeamController@members');
        Route::get('/team/matches', 'Api\TeamController@matches');
        Route::get('/team/rating', 'Api\TeamController@rating');

        Route::get('/match/round', 'Api\MatchController@round');
        Route::get('/match/tour', 'Api\MatchController@tour');

        Route::get('/champ/list', 'Api\ChampController@index');
        Route::get('/search', 'Api\SearchController@index');

        Route::get('/categories', 'Api\TeamController@categories');
        Route::get('/category/{id}/{action?}', 'Api\TeamController@category');
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
    });
    Route::group(['prefix' => 'cloudpayments','namespace' => 'Api'], function() {
        Route::match(['GET', 'POST'], 'check', 'PaymentController@webhook');
//        Route::match(['GET', 'POST'], 'pay', 'MyNotifier@pay');
    });
});
