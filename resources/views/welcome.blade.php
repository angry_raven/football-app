<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Admin Panel</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
              type="text/css">
        <link href="{{ asset('/adminPanel/global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/adminPanel/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/adminPanel/assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/adminPanel/assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/adminPanel/assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/adminPanel/assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/adminPanel/assets/css/style_admin.css') }}" rel="stylesheet" type="text/css">

        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>

    </head>
    <body>
      <div id="app"></div>
      <script src="../js/app.js"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/main/jquery.min.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>

      <script src="{{ asset('/adminPanel/global_assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>

      <script src="{{ asset('/adminPanel/assets/js/app.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_pages/dashboard.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_charts/pages/dashboard/light/streamgraph.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_charts/pages/dashboard/light/sparklines.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_charts/pages/dashboard/light/lines.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_charts/pages/dashboard/light/areas.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_charts/pages/dashboard/light/donuts.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_charts/pages/dashboard/light/bars.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_charts/pages/dashboard/light/progress.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_charts/pages/dashboard/light/heatmaps.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_charts/pages/dashboard/light/pies.js') }}"></script>
      <script src="{{ asset('/adminPanel/global_assets/js/demo_charts/pages/dashboard/light/bullets.js') }}"></script>
    </body>
</html>
