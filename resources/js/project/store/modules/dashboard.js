export const dashboard = {
    state: {
        access_token: null,
        user : null,
        getAllUser : '',
        getOneUserRe : '',
        getTeamLists : '',
        getTeamListForEdit: '',
        getAllTeamSt : '',
        getOneTeamSt : '',
        getAllResults : '',
    },

    mutations: {

        GET_TEAM_LISTS(state, payload) {
            state.getTeamLists = payload
        },

        GET_ONE_TEAM_LIST(state, payload) {
            state.getTeamListForEdit = payload
        },

        GET_USERS(state, payload) {
            state.getAllUser = payload
        },

        GET_ONE_USER(state, payload) {
            state.getOneUserRe = payload
        },

        GET_TEAM_STRUCTURE(state, payload) {
            state.getAllTeamSt = payload
        },

        GET_ONE_TEAM_STR(state, payload) {
            state.getOneTeamSt = payload
        },

        GET_ALL_RESULT(state, payload) {
            state.getAllResults = payload
        },

    },
    actions: {
        //Auth
        async loginAction(context, payload) {
            try {
                let response = await axios.post('/login', payload);
                if (response.data.access_token) {
                    let token = window.localStorage.setItem('token', response.data.access_token);
                    this.access_token = token;
                    return response.data;
                } else {
                    return response.data;
                }
            } catch (error) {
                return error;
            }
        },

        async registerAction(context, payload) {
            try {
                let response = await axios.post('/register', payload);
                if (response.data.access_token) {
                    let token = window.localStorage.setItem('token', response.data.access_token);
                    this.access_token = token;
                    return response.data;
                } else {
                    return response.data;
                }
            } catch (error) {
                return error;
            }
        },

        async logoutAction(context, payload) {
            try {
                let response = await axios.get('/logout', {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}}, payload);
                window.localStorage.removeItem('token');
                return response;
            } catch (error) {
                return error;
            }
        },

        //Result
        async getScore(context, payload) {
            try {
                let response = await axios.get('/get_results', {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}}, payload);
                context.commit('GET_ALL_RESULT', response.data.results);

                return response;
            } catch (error) {
                return error;
            }
        },

        async delResult(context, payload) {
            try {
                let response = await axios.get('/delete_result_row/' + payload, {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}});
                context.commit('GET_ALL_RESULT', response.data.results);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async addResult(context, payload) {
            try {
                let response = await axios.get('/add_new_result', {
                    params: {resultValue: payload},
                    headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}
                });
                context.commit('GET_ALL_RESULT', response.data.results);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        //Team List
        async getTeamLists(context, payload) {
            try {
                let response = await axios.get('/get_team_list', {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}});
                context.commit('GET_TEAM_LISTS', response.data.team_lists);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async addNewTeam(context, payload) {
            try {
                let formData = new FormData();
                formData.append('file', payload.file);
                formData.append('fileClub', payload.fileClub);
                formData.append('club', payload.club);
                formData.append('trainer', payload.trainer);
                let response = await axios.post('/add_team_list', formData,
                    {headers: {'Content-Type': 'multipart/form-data'}
                    });
                context.commit('GET_TEAM_LISTS', response.data.team_lists);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async deleteTeamById(context, payload) {
            try {
                let response = await axios.get('/delete_team_list/' + payload, {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}});
                context.commit('GET_TEAM_LISTS', response.data.team_lists);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async getNewEditTeamList(context, payload) {
            try {
                let formData = new FormData();
                formData.append('trainer_img', payload.trainer_img);
                formData.append('club_img', payload.club_img);
                formData.append('club', payload.club);
                formData.append('trainer', payload.trainer);
                formData.append('id', payload.id);
                let response = await axios.post('/new_edit_team_list', formData,
                    {headers: {'Content-Type': 'multipart/form-data'}
                    });
                context.commit('GET_TEAM_LISTS', response.data.team_lists);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async getResultTeamList(context, payload) {
            try {
                let response = await axios.get('/get_team_list_result/' + payload, {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}});
                context.commit('GET_ONE_TEAM_LIST', response.data);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async getAllTeamList(context, payload) {
            try {
                let response = await axios.get('/get_all_team_list', {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}});

                return response.data;
            } catch (error) {
                return error;
            }
        },

        // Team Structure
        async getAllTeamStructure(context, payload) {
            try {
                let response = await axios.get('/get_all_team_structure', {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}});
                context.commit('GET_TEAM_STRUCTURE', response.data.command_structures);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async addNewPlayer(context, payload) {
            try {
                let response = await axios.get('/add_new_player', {
                    params: {newPlayer: payload},
                    headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}
                });

                context.commit('GET_TEAM_STRUCTURE', response.data.command_structures);
                return response.data;
            } catch (error) {
                return error;
            }
        },

        async editNewResultTeamSt(context, payload) {
            try {
                let response = await axios.get('/edit_player_str', {
                    params: {editPlayer: payload},
                    headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}
                });
                context.commit('GET_TEAM_STRUCTURE', response.data.command_structures);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async getOneTeamStructure(context, payload) {
            try {

                let response = await axios.get('/get_one_str_team/' + payload, {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}});
                context.commit('GET_ONE_TEAM_STR', response.data.getNewRes);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async deleteTeamStruct(context, payload) {
            try {
                let response = await axios.get('/delete_team_str/' + payload, {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}});
                context.commit('GET_TEAM_STRUCTURE', response.data.command_structures);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        // Users
        async getUsers(context, payload) {
            try {
                let response = await axios.get('/get_users', {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}});

                context.commit('GET_USERS', response.data.users);

                return response;
            } catch (error) {
                return error;
            }
        },

        async deleteUserById(context, payload) {
            try {
                let response = await axios.get('/delete_user/' + payload, {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}}, payload);
                context.commit('GET_USERS', response.data.users);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async getUserById(context, payload) {
            try {
                let response = await axios.get('/get_one_user/' + payload, {headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}});
                context.commit('GET_ONE_USER', response.data);

                return response.data;
            } catch (error) {
                return error;
            }
        },

        async editUser(context, payload) {
            try {
                let response = await axios.get('/edit_user', {
                    params: {newUserValue: payload},
                    headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}
                });
                context.commit('GET_USERS', response.data.users);

                return response.data;
            } catch (error) {
                return error;
            }
        },

    },
    getters: {
        getToken(state) {
            return state.access_token;
        },

        getAllUsers(state) {
            return state.getAllUser;
        },

        teamLists(state) {
            return state.getTeamLists;
        },

        getForEditTeamList(state) {
            return state.getTeamListForEdit;
        },

        getNewOneUser(state) {
            return state.getOneUserRe
        },

        getAllTeamStr(state) {
            return state.getAllTeamSt
        },

        getOneTeamStruc(state) {
            return state.getOneTeamSt
        },

        allResults(state) {
            return state.getAllResults
        }

    }
};
