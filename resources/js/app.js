import axios from "axios";

require('./bootstrap');

import Vue from 'vue';
import App from './project/App.vue';
import { store } from './project/store/index';
import routes from './project/routes/route';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import Vuelidate from 'vuelidate';
import VModal from 'vue-js-modal'
import VueSweetAlert from 'vue-sweetalert'




Vue.prototype.$http = axios;

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(Vuelidate);
Vue.use(VModal);
Vue.use(VueSweetAlert)

const router = new VueRouter({
    history: true,
    routes,
})

const app = new Vue({
    el: '#app',
    router,
    store,
    render: h =>h(App),
});
