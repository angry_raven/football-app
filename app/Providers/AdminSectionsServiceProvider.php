<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\Models\Champ::class => 'App\Http\Sections\Champs',
        \App\Models\ChampTeam::class => 'App\Http\Sections\Result',
        \App\Models\Match::class => 'App\Http\Sections\Matchs',
        \App\Models\Team::class => 'App\Http\Sections\Teams',
        \App\Models\Player::class => 'App\Http\Sections\Players',
        \App\Models\Trainer::class => 'App\Http\Sections\Trainers',
        \App\Models\Category::class => 'App\Http\Sections\Category',
        \App\Models\Payment::class => 'App\Http\Sections\Payment',
        \App\Models\Stadium::class => 'App\Http\Sections\Stadium',
        \App\User::class => 'App\Http\Sections\Users',
    ];

    /**
     * Register sections.
     *
     * @param \SleepingOwl\Admin\Admin $admin
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//
        \AdminNavigation::setFromArray([
            [
                'title' => 'Выход',
                'icon' => 'fas fa-sign-out-alt',
                'priority' => 1000,
                'url' => '/logout'
            ]
        ]);

        parent::boot($admin);
    }
}
