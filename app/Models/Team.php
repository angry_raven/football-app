<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property integer $id
 * @property string $name
 * @property string $city
 * @property string $avatar
 * @property string $created_at
 * @property string $updated_at
 * @property ChampTeam[] $champTeams
 * @property MatchTeam[] $matchTeams
 * @property Player[] $players
 */
class Team extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';


    /**
     * @var array
     */
    protected $fillable = ['name', 'city', 'avatar', 'category_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function champTeams()
    {
        return $this->hasMany('App\Model\ChampTeam');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function matchTeams()
    {
        return $this->hasMany('App\Models\MatchTeam');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function players()
    {
        return $this->hasMany('App\Models\Player')
            ->select('id', 'name', DB::raw('YEAR(birthday) as birthday'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trainers()
    {
        return $this->hasMany('App\Models\Trainer')
            ->select('id', 'avatar', 'name');
    }

    function category(){
        return $this->belongsTo(Category::class);
    }
}
