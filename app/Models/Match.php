<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

/**
 * @property integer $id
 * @property integer $champ_id
 * @property integer $tour
 * @property integer $round
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 * @property Champ $champ
 * @property MatchTeam[] $matchTeams
 */
class Match extends Model
{
    public $team1;
    public $team2;
    public $goal1;
    public $goal2;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['champ_id', 'tour', 'round', 'date', 'team1', 'team2', 'goal1', 'goal2', 'stadium_id'];


    protected static function boot()
    {
        parent::boot();
        static::retrieved(function ($model) {

            if ($model->id) {
                $matchTeams = MatchTeam::where('match_id', $model->id)->get();
                $i = 1;
                foreach($matchTeams as $v) {
                    $model->attributes['team'.$i] = $v->team_id;
                    $model->attributes['goal'.$i] = $v->goal;
                    $i++;
                }
            }
        });
        static::saving(function ($model) {

            $model->team1 = $model->attributes['team1'];
            $model->team2 = $model->attributes['team2'];
            $model->goal1 = $model->attributes['goal1'];
            $model->goal2 = $model->attributes['goal2'];

            unset($model->attributes['team1']);
            unset($model->attributes['team2']);
            unset($model->attributes['goal1']);
            unset($model->attributes['goal2']);

        });
        static::saved(function ($model) {

            MatchTeam::updateOrInsert(
                ['match_id' => $model->id, 'team_id' => $model->team1],
                ['goal' => $model->goal1]
            );
            MatchTeam::updateOrInsert(
                ['match_id' => $model->id, 'team_id' => $model->team2],
                ['goal' => $model->goal2]
            );


            $champTeam1 = ChampTeam::firstOrNew([
                'champ_id' => $model->champ_id,
                'team_id' => $model->team1,
                'year' => Date::parse($model->date)->format('Y')
            ]);

            $champTeam2 = ChampTeam::firstOrNew([
                'champ_id' => $model->champ_id,
                'team_id' => $model->team2,
                'year' => Date::parse($model->date)->format('Y')
            ]);

            $team1Point = 0;
            $team2Point = 0;

            if ($model->goal1 > $model->goal2) {
                $team1Point = 3;
            } else if ($model->goal1 < $model->goal2) {
                $team2Point = 3;
            } else if ($model->goal1 == $model->goal2) {
                $team1Point = 1;
                $team2Point = 1;
            }

            $champTeam1->z = $champTeam1->z ? $champTeam1->z + $model->goal1 : $model->goal1;
            $champTeam1->p = $champTeam1->p ? $champTeam1->p + $model->goal2 : $model->goal2;
            $champTeam1->o = $champTeam1->o ? $champTeam1->o + $team1Point : $team1Point;

            $champTeam2->z = $champTeam2->z ? $champTeam2->z + $model->goal2 : $model->goal2;
            $champTeam2->p = $champTeam2->p ? $champTeam2->p + $model->goal1 : $model->goal1;
            $champTeam2->o = $champTeam2->o ? $champTeam2->o + $team2Point : $team2Point;

            $champTeam1->save();
            $champTeam2->save();

        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function champ()
    {
        return $this->belongsTo('App\Models\Champ');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function matchTeams()
    {
        return $this->hasMany('App\Models\MatchTeam');
    }

    public function match_teams()
    {
        return $this->hasMany('App\Models\MatchTeam');
    }

    public function stadium()
    {
        return $this->hasOne('App\Models\Stadium','id','stadium_id');
    }
}
