<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
      'first_club_id', 'second_club_id', 'round', 'score','start_day'
    ];

    public function first_team_list() {
        return $this->belongsTo(TeamList::class,'first_club_id','id');
    }
    public function second_team_list() {
        return $this->belongsTo(TeamList::class,'second_club_id','id');
    }
}
