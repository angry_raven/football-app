<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamList extends Model
{
    protected $fillable = [
      'club', 'trainer', 'trainer_img', 'club_img', 'champ_id', 'city'
    ];
  
    public function champ() {
        return $this->belongsTo(Champ::class,'champ_id','id');
    }

    public function matches() {
      return $this->hasMany(Result::class,'first_club_id','id')->select('id');
  }
}
