<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TeamList;
use App\Models\TeamComposition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class TeamListController extends Controller
{
    public function getAllTeamLists()
    {
        $getTeamList = TeamList::orderBy('id', 'desc')->get();

        return response()->json([
            'team_lists' => $getTeamList
        ], 201);
    }

    public function addTeamList(Request $request)
    {
        $file = $request->file->storeAs('/adminPanel/images/trainer',
            time() . '.' . $request->file->getClientOriginalExtension());
        $fileClub = $request->fileClub->storeAs('/adminPanel/images/club',
            time() . '.' . $request->fileClub->getClientOriginalExtension());
        TeamList::create([
            'club' => $request->club,
            'trainer' => $request->trainer,
            'club_img' => $fileClub,
            'trainer_img' => $file,
        ]);

        return $this->getAllTeamLists();
    }

    public function deleteTeamList($id)
    {
        $getImagesPath = TeamList::select('club_img', 'trainer_img')->where('id', $id)->get();

        if (File::delete(public_path($getImagesPath[0]->club_img))) {
            File::delete(public_path($getImagesPath[0]->club_img));
        }
        if (File::delete(public_path($getImagesPath[0]->trainer_img))) {
            File::delete(public_path($getImagesPath[0]->trainer_img));
        }
        $teamList = TeamList::find($id);
        $teamList->delete();

        return $this->getAllTeamLists();
    }

    public function getTeamListResult($id)
    {
        $getTeamlistRes = TeamList::where('id', $id)->first();

        return response()->json([
            'getListTeamResult' => $getTeamlistRes
        ], 201);
    }

    public function editTeamList(Request $request)
    {
//        delete old image from folder
        $getImagesPath = TeamList::select('club_img', 'trainer_img')->where('id', $request->id)->get();
        if($request->club_img !== null){
            if (File::delete(public_path($getImagesPath[0]->club_img))) {
                File::delete(public_path($getImagesPath[0]->club_img));
            }
            //        add new image to folder
            $club_img = $request->club_img->storeAs('/adminPanel/images/club',
                time() . '.' . $request->club_img->getClientOriginalExtension());
            TeamList::where('id', $request->id)->update([
                'club_img'=>$club_img,
            ]);
        }
        if($request->trainer_img !== null){
            if (File::delete(public_path($getImagesPath[0]->trainer_img))) {
                File::delete(public_path($getImagesPath[0]->trainer_img));
            }
            //        add new image to folder
            $trainer_img = $request->trainer_img->storeAs('/adminPanel/images/trainer',
                time() . '.' . $request->trainer_img->getClientOriginalExtension());
            TeamList::where('id', $request->id)->update([
                'trainer_img'=> $trainer_img,

            ]);
        }

        TeamList::where('id', $request->id)->update([
            'club' => $request->club,
            'trainer' => $request->trainer,
        ]);

        return $this->getAllTeamLists();
    }

    public function getAllTeamList(){
        $getTeamAllList = TeamList::select('id', 'club')->get();

        return response()->json([
           'allListTeam' =>  $getTeamAllList
        ], 200);
    }



    
    public function getTeamMembers(Request $request)
    {
        $trainer = TeamList::select(
            'id', 'trainer_img as avatar', 'trainer as name'
            )->where('id', $request->id)->get();
        
        $players = TeamComposition::select(
            'id', 'player_name as name', DB::raw('YEAR(birthday) as birthday')
            )->where('club_id', $request->id)->get();

        return response()->json([
            'trainer' => $trainer,
            'players' => $players
        ], 200);
    }

    public function getTeamWithMatches(Request $request)
    {
//$teams = TeamList::with('matches')
//->select('id', 'club as name', 'city', 'club_img as avatar')->get();
        

//        return response()->json([
//            'teams' => $teams
//        ], 200);
    }

}
