<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Team;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $s = $request->s ? $request->s : '';

        DB::enableQueryLog(); // Enable query log
        $teams = Team::select('id', 'name', 'avatar')->where('name', 'like','%'. $s.'%')->take(7)->get();

        return response()->json($teams, 200);
    }
}
