<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Result;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    public function getResults()
    {
        $getResults = Result::with('first_team_list', 'second_team_list')
            ->orderBy('id', 'desc')
            ->get();
        return response()->json([
            'results' => $getResults
        ], 201);
    }

    public function delResult($id)
    {
        $res = Result::find($id);
        $res->delete();

        return $this->getResults();
    }

    public function addResult(Request $request)
    {
        $result = json_decode($request->resultValue);
        Result::create([
            'first_club_id' => $result->club_one,
            'second_club_id' => $result->club_two,
            'round' => $result->round,
            'score' => $result->score,
            'start_day' => $result->start_day,
        ]);

        return $this->getResults();


        return $this->getTeamStr();

    }


}
