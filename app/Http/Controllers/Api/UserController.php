<?php

namespace App\Http\Controllers\Api;

use Albakov\LaravelCloudPayments\Facade;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getUsers()
    {
        $getUsers = User::orderBy('id', 'desc')->get();

        return response()->json([
            'users' => $getUsers
        ], 201);
    }

    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->delete();

       return $this->getUsers();
    }


    public function getOneUser($id)
    {
        $getOneUserRes = User::select('id', 'phone')->where('id', $id)->orderBy('id', 'desc')->first();

        return response()->json([
            'getOneUserResult' => $getOneUserRes
        ], 201);
    }

    public function editUser(Request $request)
    {
       $val = json_decode($request->newUserValue);

        User::where('id', $val->id)->update([
            'phone' => $val->phone,
        ]);

        return $this->getUsers();
    }

}
