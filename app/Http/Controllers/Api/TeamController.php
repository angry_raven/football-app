<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\ChampTeam;
use App\Models\Match;
use App\Models\Team;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Jenssegers\Date\Date;
use RuntimeException as RuntimeExceptionAlias;

class TeamController extends Controller
{
    /**
     * .
     *
     * @return \Illuminate\Http\Response
     */
    public function members(Request $request)
    {
        //
        $team = Team::findOrFail($request->id);

        return response()->json([
            'trainer' => $team->trainers[0],
            'players' => $team->players,
        ], 200);
    }

    /**
     * .
     *
     * @return \Illuminate\Http\Response
     */
    public function rating(Request $request)
    {
        //
        $validData = $request->validate([
            'from' => 'integer',
            'to' => 'integer',
            'champType' => 'integer|required',
        ]);

        $champTeams = ChampTeam::with('team');
        $champTeams = $champTeams->where('champ_id', $validData['champType']);
        if (isset($validData['from'])) {
            $champTeams = $champTeams->where('year', '>=', $validData['from']);
        } else {
            if (isset($validData['from'])) {
                $champTeams = $champTeams->where('year', '<=', $validData['to']);
            }
        }
        $champTeams = $champTeams->orderBy('o', 'desc')->paginate(7);

        $r = [];
        foreach ($champTeams as $k => $v) {
            $r[] = [
                'num' => $k,
                'team' => [
                    'id' => $v->team->id,
                    'avatar' => $v->team->avatar,
                    'name' => $v->team->name,
                ],
                'z' => $v->z,
                'p' => $v->p,
                'o' => $v->o,
            ];
        }

        return response()->json();
    }

    /**
     * Список категоирй
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function categories()
    {
        return response()->json(Category::query()->has('teams')->orderBy('name', 'ASC')->get());
    }

    /**
     * Список данных по команде
     *
     * @param $id
     * @param $action
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */

    public function category(Request $request, $id, $action): ?JsonResponse
    {
        if (! $id) {
            return response()->json(['success' => false, 'error' => ['message' => 'ID field required']]);
        }
        if (! $action) {
            return response()->json(['success' => false, 'error' => ['message' => 'ACTION field required']]);
        }

        $validData = $request->validate([
            'champType' => 'integer|required',
        ]);

        $category = Category::findOrFail($id);
        $team = Team::whereCategoryId($category->id)->get();
        switch ($action) {
            case('teams'):
                return response()->json(Team::whereCategoryId($category->id)->paginate(20));
                break;
            case('matches'):
                return response()->json($this->matches($team->pluck('id')));
                break;
            case('tour'):
                return response()->json($this->tour($request, $team->pluck('id'), $validData['champType']));
                break;
            case('round'):
                return response()->json($this->round($request, $team->pluck('id'), $validData['champType']));
                break;
            case('rating'):
                return response()->json($this->ratingApi($request, $team->pluck('id'), $validData['champType']));
                break;
            default:
                throw new RuntimeExceptionAlias('Unexpected value');
        }

        return response()->json(['success' => false, 'error' => ['message' => 'An error occurred']]);
    }

    /**
     * .
     *
     * @param null $id
     * @return \Illuminate\Http\Response
     */
    public function matches($id = null)
    {
        //
        $r = [];
        if (! $id) {
            $teams = Team::query()->paginate(7);
        } else {
            $teams = Team::query()->whereIn('id', $id)->paginate(20);
        }
        $matchs = Match::with('matchTeams', 'matchTeams.team', 'matchTeams.team')->get();

        foreach ($teams as $team) {
            $matches = [];
            foreach ($matchs as $m) {
                $findMatchId = false;
                foreach ($m->matchTeams as $mi) {
                    if ($mi->team_id == $team->id) {
                        $findMatchId = true;
                    }
                }

                if ($findMatchId) {
                    $matchId = -1;
                    $matchTeams = [];
                    $date = '';
                    foreach ($m->matchTeams as $mi) {
                        $matchId = $mi->match_id;
                        $matchTeams[] = [
                            'id' => $mi->team_id,
                            'name' => $mi->team->name,
                            'avatar' => $mi->team->avatar,
                        ];
                        $date = $m->date;
                    }

                    $matches[] = [
                        'id' => $m->id,
                        'teams' => $matchTeams,
                        'date' => Date::parse($m->date)->format('j M'),
                    ];
                }
            }

            $r[] = [
                'team' => [
                    'id' => $team->id,
                    'name' => $team->name,
                    'city' => $team->city,
                    'avatar' => $team->avatar,
                ],
                'matches' => $matches,
            ];
        }
        if (! $id) {
            return response()->json([
                'teams' => $r,
            ], 200);
        } else {
            return $r;
        }
    }

    public function tour(Request $request, $id, $champType)
    {

        $matchs = Match::with(['matchTeams', 'matchTeams.team'])
            ->where('champ_id', $champType)
            ->orderBy('tour', 'desc')
        ->get();

        $r = [];
        foreach ($matchs as $v) {
            $teams = [];
            $date = Date::parse($v->date)->format('j M');
            if ($id->count()) {
                foreach ($v->matchTeams as $team) {
                    foreach ($id as $i) {
                        if ($team->team->id !== $i) {
                            continue;
                        }

                            $teams[] = [
                                'id' => $team->team->id,
                                'name' => $team->team->name,
                                'avatar' => $team->team->avatar,
                            ];

                    }
                }

                if ($teams != []) {
                    if (count($teams) != 1) {
                        if (!isset($r[$v->tour])) {
                            $r[$v->tour] = ['tour' => $v->tour, 'matches' => []];
                        }
                        $r[$v->tour]['matches'][] = [
                            'teams' => $teams,
                            'date' => $date,
                            'stadium' => $v->stadium
                        ];
                    }
                }
            }
        }
        $data = [];
        foreach ($r as $key => $item)
        {
            $data[] = $item;
        }

        $count = count($data);
        $currentPage = $request->page;
        $paginator = new LengthAwarePaginator($data, $count,20,$currentPage);

        return $paginator->items();
    }

    public function round(Request $request, $id, $champType)
    {
        $matchs = Match::with(['matchTeams', 'matchTeams.team'])
            ->where('champ_id', $champType)
            ->orderBy('tour', 'desc')
            ->get();

        $r = [];
        foreach ($matchs as $v) {
            $teams = [];
            $score = [];
            $date = Date::parse($v->date)->format('j M');
            if ($id->count()) {
                foreach ($v->matchTeams as $team) {
                    foreach ($id as $i) {
                        if ($team->team->id !== $i) {
                            continue;
                        }

                        $teams[] = [
                            'id' => $team->team->id,
                            'name' => $team->team->name,
                            'avatar' => $team->team->avatar,
                        ];
                        $score[] = $team->goal;
                    }
                }
                if ($teams != []) {
                    if (count($teams) != 1) {
                        if (!isset($r[$v->round])) {
                            $r[$v->round] = ['round' => $v->round, 'matches' => []];
                        }
                        $r[$v->round]['matches'][] = [
                            'teams' => $teams,
                            'date' => $date,
                            'score' => $score,
                        ];
                    }
                }
            }
        }
        $data = [];
        foreach ($r as $key => $item)
        {
            $data[] = $item;
        }

        $count = count($data);
        $currentPage = $request->page;
        $paginator = new LengthAwarePaginator($data, $count,20,$currentPage);

        return $paginator->items();
    }

    public function ratingApi(Request $request, $id, $champType)
    {
        $champTeams = ChampTeam::with('team')->orderBy('o', 'desc');
        $champTeams = $champTeams->where('champ_id', $champType)->get();
        $r = [];
        if ($id->count()) {
            foreach ($champTeams as $k => $v) {
                foreach ($id as $i) {
                    if ($v->team->id !== $i) {
                        continue;
                    }

                    $r[] = [
                        'num' => $k,
                        'team' => [
                            'id' => $v->team->id,
                            'avatar' => $v->team->avatar,
                            'name' => $v->team->name,
                        ],
                        'z' => $v->z,
                        'p' => $v->p,
                        'o' => $v->o,
                    ];
                }
            }
            $data =  $r;
            $count = count($data);
            $currentPage = $request->page;
            $paginator = new LengthAwarePaginator($data, $count,20,$currentPage);

            return $paginator->items();
        }

        return [];
    }
}
