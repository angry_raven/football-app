<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;

/**
 * Class Matchs
 *
 * @property \App\Models\Match $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Matchs extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->title = 'Матчи';
        $this->addToNavigation()->setPriority(3)->setIcon('fas fa-futbol');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('champ.name', 'Чемпионат'),
            AdminColumn::text('stadium.name', 'Стадион'),
            AdminColumn::text('tour', 'Тур'),
            AdminColumn::text('round', 'Раунд'),
            AdminColumn::text('matchTeams.team', 'Команды')
            ->setModifier(function ($value, $model) {
                $value = json_decode($value, true);
                $r = [];
                foreach($value as $team){
                    $r[] = $team['name'];
                }
                return implode(' - ', $r);
            }),
            AdminColumn::text('matchTeams.goal', 'Счет'),
            AdminColumn::datetime('date', 'Дата матча')->setFormat('d.m.Y'),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->setDisplaySearch(false)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;


        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::select('champ_id', 'Чемпионат', \App\Models\Champ::class)->setDisplay('name')->required(),
            AdminFormElement::select('stadium_id', 'Стадион', \App\Models\Stadium::class)->setDisplay('name')->required(),
            AdminFormElement::text('tour', 'Тур')->required(),
            AdminFormElement::text('round', 'Раунд')->required(),
            AdminFormElement::date('date', 'Дата')->required(),
            AdminFormElement::select('team1', 'Команда 1', \App\Models\Team::class)
                ->setDisplay('name')->required(),
            AdminFormElement::text('goal1', 'Голы команды 1')
                ->required(),
            AdminFormElement::select('team2', 'Команда 2', \App\Models\Team::class)
                ->setDisplay('name')->required(),
            AdminFormElement::text('goal2', 'Голы команды 2')
                ->required(),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
