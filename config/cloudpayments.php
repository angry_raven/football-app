<?php

return [
    'apiSecret' => 'b627ebdabff22ee41c959a560f6f1c1c',
    'publicId' => 'pk_9aa0ecb2820d3c32970e42c5c32b1',
    'apiUrl' => 'https://api.cloudpayments.ru',
    'cultureName' => 'ru-RU', // https://cloudpayments.ru/Docs/Api#language
];
