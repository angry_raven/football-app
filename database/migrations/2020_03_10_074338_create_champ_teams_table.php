<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChampTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champ_teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('champ_id');
            $table->unsignedBigInteger('team_id');
            $table->unsignedSmallInteger('z');
            $table->unsignedSmallInteger('p');
            $table->unsignedSmallInteger('o');
            $table->timestamps();

            $table->foreign('team_id')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade');

            $table->foreign('champ_id')
                ->references('id')
                ->on('champs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champ_teams');
    }
}
